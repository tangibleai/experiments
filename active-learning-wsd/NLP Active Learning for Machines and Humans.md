---
jupyter:
  jupytext:
    formats: ipynb,md
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.3'
      jupytext_version: 1.13.4
  kernelspec:
    display_name: Python 3 (ipykernel)
    language: python
    name: python3
---

# Slides


## Algorithm


### Inputs

* C: Supervised **C**lassifier (e.g. `LogisiticRegression`)
* T: **T**rainingSet -- few (\~10) labeled examples
* U: **U**labeledSet -- many (~10M) labeled examples
* H: **H**uman oracle -- high quality labeler 
* A: Unsupervised **A**nnomalyDetector
* V: **V**alidationSet

If the learning is not perpetual:

* accuracy_threshold: Desired validation accuracy for the **C**lassifier


#### Repeat Indefinitely:

```python
while True:
    pass
```

1. Train **C**lassifier on **T**rainingSet

```python
    C = C.fit(T)
```

2. Evaluate **C**lassifier on **V**alidationSet and decide if done

```python
    if C.score(V) > required_accuracy:
        break
```

3. Estimate confidence and density (difficulty) for **U**nlabeledData with **C**lassifier and **A**nnomalyDetector

```python
    U['difficulty'] = C.predict_proba(U) * (1 - A.predict_proba(U))
```

4. Select **D**ifficultExamples based on uncertainty and density

```python
    D = U.sort_values('uncertainty+density')[-10:]  
```

5. Split **D**ifficultData into **D**ifficult**T**rainingData and **D**ifficult**V**alidationData

```python
    DT, DV = split(H.predict(D))
```

6. Augment **T**rainingData with newly labeled **D**ifficult**T**rainingData

```python
    T.extend(DT)
    V.extend(DV)
```

### Inputs

* C: Supervised **C**lassifier (e.g. `LogisiticRegression`)
* T: **T**rainingSet -- few (\~10) labeled examples
* U: **U**labeledSet -- many (~10M) labeled examples
* H: **H**uman oracle -- high quality labeler 
* A: Unsupervised **A**nnomalyDetector
* V: **V**alidationSet

If the learning is not perpetual:

* accuracy_threshold: Desired validation accuracy for the **C**lassifier

```python
from sklearn.linear_model import LogisticRegression
C = LogisticRegression()
```

### Repeat Indefinitely:

```python
while True:
    C.fit(T)
    if C.score(V) > required_accuracy:
        break
    U['difficulty'] = C.predict_proba(U) * estimate_density(U)
    D = U.sort_values('difficulty')[-10:]
    T.extend(DT)
    V.extend(DV)
```
