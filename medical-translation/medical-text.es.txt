TransBTS: tumor cerebral multimodal Segmentación mediante transformador
Wenxuan Wang1, Chen Chen2, Meng Ding3, Hong Yu1, Sen Zha1, Jiangyun Li1,†
1 Escuela de Ingeniería Automática y Eléctrica, Universidad de Ciencias y Tecnología Pekín, China, s20200579@xs.ustb.edu.cn, g20198754@xs.ustb.edu.cn, g20198675@xs.ustb.edu.cn, leejy@ustb.edu.cn
2 Centro de Investigación en Visión por Computador, Universidad de Florida Central, EE. UU., chen.chen@ucf.edu
3 Scoop Medical, Houston, TX, EE. UU., meng.ding@okstate.edu
† Autor para correspondencia: Jiangyun Li
Abstracto. Transformador, que puede beneficiarse de la integración global (de largo alcance) modelado de formaciones usando mecanismos de autoatención, ha sido exitoso-ful en procesamiento de lenguaje natural y clasificación de imágenes 2D recientemente. Sin embargo, tanto las características locales como las globales son cruciales para la predicción densa. tareas de segmentación, especialmente para la segmentación de imágenes médicas en 3D. En este papel, por primera vez explotamos Transformer en 3D CNN para MRI Brain Tumor Segmentation y proponer una red novedosa llamada TransBTS basado en la estructura codificador-decodificador. Para capturar el contenido 3D local información de texto, el codificador primero utiliza 3D CNN para extraer el volumen mapas métricos de características espaciales. Mientras tanto, los mapas de características se reforman elaboradamente para tokens que se introducen en Transformer para funciones globales modelado. El decodificador aprovecha las características integradas por Transformer y realiza un muestreo ascendente progresivo para predecir la segmentación detallada mapa de tación. Amplios resultados experimentales en BraTS 2019 y Los conjuntos de datos de 2020 muestran que TransBTS logra resultados comparables o superiores resultados que los métodos 3D de vanguardia anteriores para la segmentación de tumores cerebrales Mención en resonancias magnéticas 3D. El código fuente está disponible en https://github.com/Wenxuan-1119/TransBTS.
Palabras clave: Segmentación · Tumor cerebral · MRI · Transformador · CNN 3D
## Introducción
Los gliomas son los tumores cerebrales malignos más comunes con diferentes niveles de agresividad. Segmentación automatizada y precisa de estos tumores malignos en La resonancia magnética nuclear (RMN) es de vital importancia para el diagnóstico clínico. Las redes neuronales convolucionales (CNN) han logrado un gran éxito en varios tareas de visión como clasificación, segmentación y detección de objetos. Completamente Las redes convolucionales (FCN) [10] realizan una segmentación semántica de extremo a extremo por primera vez con resultados impresionantes. U-Net [15] utiliza un codificador simétrico estructura del decodificador con conexiones de salto para mejorar la retención de detalles, convirtiéndose


- Las mutaciones de ADN 
- Secuencia ADN
- RCP (PCR)
- PCA
- la canalización de datos 
- transformador 
- `tabla de datos` vs Dask DataFrame
- Pipenv & Poesía
- setuptools and Anaconda 
- herramientas de configuración y Anaconda
- Convertir dosis a unidades facturables
- Enviar datos de facturación a los proveedores de seguros
- Reclamaciones laborales y denegaciones de reclamaciones para garantizar el máximo reembolso por los servicios prestados
- Realizar revisiones y auditorías de Medicare
- Facturación de Medicaid
- Certificación HIPPA
- Cumplimiento de FERPA 
- familiaridad con el océano digital
- CVE y Ciberseguridad 