"""
Interrater Reliability: The overall inter-rater reliability was .73.
The lowest reliability was for counterclaims and rebuttals (which were often labeled as claims).
The highest reliability was for concluding statements.
All disagreements were adjudicated by an expert rater.

Prediction strings:
The `predictionstring` column takes the full text and the `discourse_start` and `discourse_end`
character indices to create the list of word indices.
The code used to generate `predictionstring` is below:

Label mismatch:
Many of you noted the discrepancies between the columns discourse_start/discourse_end, discourse_text, and predictionstring.
There is noise in both the original and Kaggle-created labels.
The discrepancies are minor but observable enough to raise valid questions and concern.
Overall, rely on predictionstring as your target column, because this column determines your LB score.

The discourse_start/discourse_end columns refer to the original boundaries of the discourse text
during the human annotation process and prior to internal PII masking.
PII masks replace personally identifying information with generic labels (e.g., the student name 'John Doe'
is replaced with GENERIC_NAME or PROPER_NAME).
Such texts will have slightly different boundaries than what the character indices in
`discourse_start`/`discourse_end` indicate.
Please note these affected texts make up less than 0.1% of the dataset,
and upon further examination, we found this did not impact scoring.
"""


def calc_word_indices(full_text, discourse_start, discourse_end):
    start_index = len(full_text[:discourse_start].split())
    token_len = len(full_text[discourse_start:discourse_end].split())
    output = list(range(start_index, start_index + token_len))
    if output[-1] >= len(full_text.split()):
        output = list(range(start_index, start_index + token_len - 1))
    return output
