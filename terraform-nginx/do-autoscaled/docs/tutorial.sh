# https://www.digitalocean.com/community/tutorials/how-to-use-terraform-with-digitalocean
# https://www.digitalocean.com/community/tutorials/how-to-structure-a-terraform-project
workon experiments
mkdir devops
more .env
cd devops
terraform --help
nano .env
source .env
export DO_PAT="$DO_PAT"
mv variables.tf ~/.ssh/variables.do.tf
export TF_LOG=1
echo "Make sure you navigate to Digital ocean and ..."
echo 0. log into the correct Digital Ocean organization account
sleep 1
echo 1. click on profile
sleep 1
echo 2. select the team
sleep 1
echo 3. select the project
sleep 1
echo 4. click settings and make it the default project
sleep 1
read -p "Press enter to continue with `terraform init`"
terraform init
nano nginx-1.tf

ssh-keygen -f ~/.ssh/id_rsa_do_terraform
echo "!!!!!!!! IMPORTANT !!!!!!!!!!!"
echo "Visit https://discoposse.com/2017/02/08/adding-ssh-access-for-digitalocean-when-using-terraform/"
echo "which will tell you to go to your account settings on DO and cat the public ssh key that you created above."
echo "1. ssh-keygen -N "" -f ~/.ssh/id_rsa_do_terraform"
echo "2. cat ~/.ssh/id_rsa_do_terraform"
echo '3. create an ssh key on your digital ocean account with the name "terraform"'
echo "4. paste that public key"

echo
read -p "Hit ENTER to terraform plan"
terraform plan -var "do_token=${DO_PAT}" -var "pvt_key=$HOME/.ssh/id_rsa_do_terraform"

echo
read -p "Hit ENTER to terraform apply"
terraform apply -var "do_token=${DO_PAT}" -var "pvt_key=$HOME/.ssh/id_rsa_do_terraform"

terraform show terraform.tfstate


echo "If you need to change some of the plan files you want to refresh your infrastructure state:"
echo 'terraform refresh -var "do_token=${DO_PAT}" -var "pvt_key=$HOME/.ssh/id_rsa_do_terraform"'
echo
echo "If you want to see the IP addresses etc:"
echo 'terraform show terraform.tfstate'
echo

# tree
# .
# └── tf/
#     ├── versions.tf
#     ├── variables.tf
#     ├── provider.tf
#     ├── droplets.tf
#     ├── dns.tf
#     ├── data-sources.tf
#     └── external/
#         └── name-generator.py

# more complex structure
# tree
# .
# └── tf/
#     ├── modules/
#     │   ├── network/
#     │   │   ├── main.tf
#     │   │   ├── dns.tf
#     │   │   ├── outputs.tf
#     │   │   └── variables.tf
#     │   └── spaces/
#     │       ├── main.tf
#     │       ├── outputs.tf
#     │       └── variables.tf
#     └── applications/
#         ├── backend-app/
#         │   ├── env/
#         │   │   ├── dev.tfvars
#         │   │   ├── staging.tfvars
#         │   │   ├── qa.tfvars
#         │   │   └── production.tfvars
#         │   └── main.tf
#         └── frontend-app/
#             ├── env/
#             │   ├── dev.tfvars
#             │   ├── staging.tfvars
#             │   ├── qa.tfvars
#             │   └── production.tfvars
#             └── main.tf