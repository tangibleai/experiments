# provider.tf
# resource "digitalocean_ssh_key" "id_rsa_do_tanbot" {
#   name       = "id_rsa_do_terraform"
#   public_key = file(var.pvt_key)
# }

terraform {
  required_providers {
    digitalocean = {
      source = "digitalocean/digitalocean"
      version = "~> 2.0"
    }
  }
}

variable "do_token" {}
variable "pvt_key" {}

provider "digitalocean" {
  token = var.do_token
}

# !!!!!!!!!!!!! IMPORTANT !!!!!!!!!!!!!!!!
# 1. ssh-keygen -N "" -f ~/.ssh/id_rsa_do_terraform
# 2. cat ~/.ssh/id_rsa_do_terraform
# 3. create an ssh key on your digital ocean account with the name "terraform"
# 4. paste that public key
data "digitalocean_ssh_key" "terraform" {
  name = "terraform"
}

